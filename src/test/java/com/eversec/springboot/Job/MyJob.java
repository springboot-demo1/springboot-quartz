package com.eversec.springboot.Job;/**
 * Created by 恒安 on 2017/6/8.
 */

import org.quartz.*;

import java.util.Date;

/**
 * 作业类
 *
 * @author lifl
 * @create 2017-06-08 9:41
 **/
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class MyJob implements Job {
    private int count;

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        JobDataMap jobDataMap = context.getJobDetail().getJobDataMap();
//        int count = jobDataMap.getInt("count");//提供属性以及set方法，不需要该代码
        count++;
        jobDataMap.put("count", count);
        System.out.println(new Date() + "==" + context.getJobDetail().getJobClass() + "==执行次数：" + count);
    }

    public void setCount(int count) {
        this.count = count;
    }
}
