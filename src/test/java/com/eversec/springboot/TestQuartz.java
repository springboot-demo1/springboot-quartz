package com.eversec.springboot;/**
 * Created by 恒安 on 2017/6/8.
 */

import com.eversec.springboot.Job.MyJob;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * 测试Quartz
 *
 * @author lifl
 * @create 2017-06-08 9:35
 **/
public class TestQuartz {
    private static SchedulerFactory factory;
    private static Scheduler scheduler;

    static {
        factory = new StdSchedulerFactory();
        try {
            scheduler = factory.getScheduler();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        // define the job and tie it to our HelloJob class
        JobDataMap jobDataMap = new JobDataMap();
        jobDataMap.put("count", 1);
        JobDetail jobDetail = newJob(MyJob.class)
                .withIdentity("myJob", "group1")
                .setJobData(jobDataMap)
                .build();
        // Trigger the job to run now, and then every 40 seconds
        Trigger trigger = newTrigger()
                .withIdentity("myTrigger", "group1")
                .startNow()
                .withSchedule(simpleSchedule()
                        .withIntervalInSeconds(1)
                        .repeatForever())
                .build();
        // Tell quartz to schedule the job using our trigger
        try {
            scheduler.scheduleJob(jobDetail, trigger);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }

        try {
            scheduler.start();
            //close();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }


    public static void close() {
        try {
            scheduler.shutdown(true);
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
}
