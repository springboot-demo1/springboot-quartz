package com.eversec.springboot.controller;/**
 * Created by 恒安 on 2017/5/27.
 */


import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

/**
 * jsp页面测试类
 *
 * @author lifl
 * @create 2017-05-27 14:58
 **/
@Controller
public class HelloController {
    @Value("${application.hello:Hello Angel}")
    private String hello;

    @RequestMapping(value = "/helloJsp")
    public String helloJsp(Map<String, Object> map) {//该map位于此处，才能在jsp页面使用
        System.out.println("HelloController.helloJsp().hello=" + hello);
        map.put("hello", hello);
        return "helloJsp";
    }

    @RequestMapping(value = "/hello")
    @ResponseBody
    public String hello() {
        return "hello";
    }
}
